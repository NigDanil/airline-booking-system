[Содержание](content.md)&emsp;[&larr; назад](1_2.md) &brvbar; [вперед &rarr;](1_4.md)

<hr>

<h1>Диаграмма классов СБ АБ</h1>
 
<p>Диаграмма классов <b>СБ АБ</b> описывает структуру классов <b>СБ АБ</b>, их атрибуты, операции (или методы) и отношения между объектами. Основными классами <b>СБ АБ</b> являются <b>Bike, Booking, Repair Bike, Insurance, Customer, Payment</b>.
</p>

<h4>Классы диаграммы классов СБ АБ</h4>

Список управления операциями:

- <b>Ticket:</b> билетов
- <b>Airlines Booking:</b> бронирования авиалиний
- <b>Passenger:</b> пассажирами
- <b>Booking Enquiry:</b> запросами на бронирование
- <b>Airline Enquiry:</b> запросами авиалинии

<h4>Классы и их атрибуты диаграммы классов СБ АБ:
</h4>

Список атрибутов:

- <b>Ticket:</b>

  - Атрибуты:
    - ticket_id,
    - ticket_customer_id,
    - ticket_type,
    - ticket_date,
    - ticket_description.

- <b>AirlinesBooking:</b>

  - Атрибуты:
    - airlines_booking_id,
    - airlines_passenger_id,
    - airlines_booking_type,
    - airlines_booking_date,
    - airlines_booking_description.

- <b>Passenger</b>:

  - Атрибуты:
    - passenger_id,
    - passenger_name,
    - passenger_mobile,
    - passenger_email,
    - passenger_username,
    - passenger_password,
    - passenger_address.

- <b>AirlineEnquiry:</b>

  - Атрибуты:
    - airline_enquiry_id,
    - airline_enquiry_title,
    - airline_enquiry_type,
    - airline_enquiry_date,
    - airline_enquiry_description.

- <b>BookingEnquiry:</b>

  - Атрибуты:
    - booking_enquiry_id,
    - booking_enquiry_title,
    - booking_enquiry_type,
    - booking_enquiry_date,
    - booking_enquiry_description.

<h4>Классы и их методы диаграммы классов СБ АБ
</h4>

- <b>Ticket:</b>

  - addTicket(),
  - editTicket(),
  - deleteTicket(),
  - searchTicket().

- <b>AirlinesBooking:</b>

  - addAirlines Booking(),
  - editAirlines Booking(),
  - deleteAirlines Booking(),
  - searchAirlines Booking()

- <b>Passenger:</b>

  - addPassenger(),
  - editPassenger(),
  - deletePassenger(),
  - searchPassenger()

- <b>BookingEnquiry:</b>

  - addBookingEnquiry(),
  - editBookingEnquiry(),
  - deleteBookingEnquiry(),
  - searchBookingEnquiry().

- <b>AirlineEnquiry:</b>

  - addAirlineEnquiry(),
  - editAirlineEnquiry(),
  - deleteAirlineEnquiry(),
  - searchAirlineEnquiry().

<h4>Диаграмма классов СБ АБ</h4>

<center>

![title class](../img/1_3.drawio.svg)

</center>

<center><b>Диаграмма классов СБ АБ</b></center>

<hr>

[Содержание](content.md)&emsp;[&larr; назад](1_2.md) &brvbar; [вперед &rarr;](1_4.md)
