<h4>Clone Project</h4>

- [ ] Clone with SSH

```bash
git@gitlab.com:NigDanil/airline-booking-system.git
```

- [ ] Clone with HTTPS

```bash
https://gitlab.com/NigDanil/airline-booking-system.git
```
---


<center><h1>Cистема бронирования авиабилетов<br>
(СБ АБ)</h1></center>

<center>

[Содержание](data/content.md)

</center>
